# EventType: 26265051
#
# Descriptor: [ Xi_cc+ -> (Lambda_c+ -> p K- pi+) K- pi+ ]cc
#
# NickName: Xicc+_LcKpi,pKpi-res=Xicc_LcKpi,pKpi-res=GenXicc,DecProdCut,m=3519MeV,WithMinPT3,MinDaughterPT200
#
# Production: GenXicc
#
# Cuts: XiccDaughtersInLHCbAndWithMinPT
# 
# CutsOptions: MinXiccPT 3000*MeV MinDaughterPT 200*MeV
#
# ParticleValue: "Xi_cc+                502        4412   1.0      3.51870000      8.000000e-14                    Xi_cc+        4412      0.00000000", "Xi_cc~-               503       -4412  -1.0      3.51870000      8.000000e-14               anti-Xi_cc-       -4412      0.00000000" 
#
# CPUTime: < 1 min
# Documentation: decay file of Xi_cc+ -> (Lambda_c+ -> p K- pi+) K- pi+, 
# using dedicated GenXicc package for production, phase space decay model used, 
# the mass of Xi_cc+ is set to 3.5187 GeV, the lifetime is set to 80 fs,
# all daughters of Xicc are required to be in the acceptance of LHCb and with PT>200 MeV
# and the Xicc PT is required to be larger than 3000 MeV, 
# EndDocumentation
#
# PhysicsWG: Charm 
# Tested: Yes
# Responsible: Ao Xu
# Email: ao.xu@cern.ch
# Date: 20180222
#
Alias MyLambda_c+ Lambda_c+
Alias Myanti-Lambda_c- anti-Lambda_c-
ChargeConj MyLambda_c+ Myanti-Lambda_c-
#
# Define Lambda(1520)0
Alias      MyLambda(1520)0       Lambda(1520)0
Alias      Myanti-Lambda(1520)0  anti-Lambda(1520)0
ChargeConj MyLambda(1520)0       Myanti-Lambda(1520)0
#
# Define K*0
Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0
#
# Define Delta++
Alias      MyDelta++      Delta++
Alias      Myanti-Delta-- anti-Delta--
ChargeConj MyDelta++      Myanti-Delta--
#
Decay Xi_cc+sig
  1.00   MyLambda_c+  K-	pi+                  PHSP;
Enddecay
CDecay anti-Xi_cc-sig
#
Decay MyLambda_c+
    0.02800         p+      K-      pi+          PHSP;
    0.01065         p+      Myanti-K*0           PHSP;
    0.00860         MyDelta++ K-                 PHSP;
    0.00414         MyLambda(1520)0 pi+          PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
Decay MyK*0
    0.6657      K+  pi-                          VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyLambda(1520)0
  0.23   p+     K-                             PHSP;
Enddecay
CDecay Myanti-Lambda(1520)0
#
Decay MyDelta++
  1.000 p+ pi+ PHSP;
Enddecay
CDecay Myanti-Delta--
#
End

