# EventType: 27573009
#
# Descriptor: [D*(2010)+ -> (D0 -> {pi- mu+ nu}) pi+]cc
#
# NickName: Dst_D0pi,pimunu=TightCut,FSRinc
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay = '^[D*(2010)+ => ^(D0 => ^pi- ^mu+ nu_mu) pi+]CC'
# tightCut.Cuts      =    {
#     '[mu+]cc'   : ' goodMuon  ' ,
#     '[pi-]cc'    : ' goodPion  ' ,
#     '[D0]cc'    : ' goodD     ' ,
#     '[D*(2010)+]cc'   : ' goodDst & goodSlowPion  ', }
# tightCut.Preambulo += [
#     'inAcc     = in_range ( 0.005 , GTHETA , 0.400 ) & in_range( 1.9 , GETA , 5.0 )' ,
#     'goodMuon  = ( GPT > 500  * MeV ) & ( GP > 6 * GeV )     & inAcc   ' ,
#     'goodPion  = ( GPT > 300  * MeV ) & ( GP > 2 * GeV ) & inAcc  ' ,
#     'goodSlowPion  = GCHILDCUT (  ( GPT > 300  * MeV ) & ( GP > 1000  * MeV ) & inAcc , "[D*(2010)+ =>  Charm ^pi+]CC"  )',
#     'goodD     = ( GPT > 250  * MeV ) & ( GP > 3 * GeV )',
#     'goodDst   = ( GPT > 500  * MeV ) & ( GP > 1 * GeV )', ]
#
# EndInsertPythonCode
#
#
# Documentation: Forces the D* decay to D0 pi+, with the D0 to pi mu nu. Tight cuts.
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Lorenzo Capriotti
# Email: lorenzo.capriotti@cern.ch
# Date: 20180926
# CPUTime: < 1 min
#

Alias MyD0 D0
Alias MyantiD0 anti-D0
ChargeConj MyD0 MyantiD0

Decay D*+sig
  1.000 MyD0  pi+    VSS;
Enddecay
CDecay D*-sig

Decay MyD0
  1.000 pi- mu+ nu_mu PHOTOS ISGW2;  
Enddecay
CDecay MyantiD0
#
End

