# EventType: 13574086
# 
# Descriptor: [B_s0 -> (D_s- -> (phi -> K+ K-) e- anti-nu_e) mu+ nu_mu]cc
#
# NickName: Bs_Dsmunu,phienu=VisibleInAcceptance,HighVisMass
# Cuts: LoKi::GenCutTool/HighVisMass
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool(LoKi__GenCutTool ,'HighVisMass')
# #
# tightCut = gen.SignalRepeatedHadronization.HighVisMass
# tightCut.Decay   = '[^(B_s0 => ^(D_s- => ^(phi(1020) => ^K+ ^K-) ^e- ^nu_e~) ^mu+ ^nu_mu)]CC'
# tightCut.Cuts    =    {
#     '[K+]cc'     : "inAcc",
#     '[pi-]cc'    : "inAcc",
#     '[e-]cc'     : "inAcc",
#     '[mu+]cc'     : "inAcc",
#     '[B_s0]cc'     : "visMass" }
# tightCut.Preambulo += [
#     "inAcc   = in_range ( 0.005 , GTHETA , 0.400 ) " ,
#     "visMass  = ( ( GMASS ( 'e-' == GID , 'mu+' == GID, 'K+' == GID, 'K-' == GID ) ) > 4500 * MeV ) " ]
# EndInsertPythonCode
#
# Documentation: background for B0s -> phi e mu LFV search
# selected to have a visible mass larger than 4.5 GeV
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Stefania Ricciardi 
# Email: stefania.ricciardi@stfc.ac.uk
# Date: 20170706
# CPUTime: 60 min
#

Alias My_phi   phi 
ChargeConj My_phi  My_phi 

Alias MyD_s+ D_s+
Alias MyD_s- D_s-
ChargeConj MyD_s+ MyD_s-

Decay B_s0sig
  1.000        MyD_s- mu+ nu_mu  PHOTOS ISGW2;
Enddecay
CDecay anti-B0sig
#
Decay MyD_s-
1.000         My_phi e- anti-nu_e PHOTOS ISGW2;
Enddecay
CDecay MyD_s+
#
Decay My_phi  
1.000          K+ K-         VSS;
Enddecay           
#
End
#
