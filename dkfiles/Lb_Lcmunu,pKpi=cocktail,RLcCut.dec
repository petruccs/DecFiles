# EventType: 15874003
#
# Descriptor: [ Lambda_b0 ==>  (Lambda_c+ ==> p+ K- pi+)  mu- anti_nu_mu   ]cc
#
# NickName: Lb_Lcmunu,pKpi=cocktail,RLcCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# CPUTime:  3 min
#
# Documentation: Lb -> Lc mu nu_mu with Lc->p K pi.  Includes acceptance, P and PT cuts on the p and mu. 
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# Generation().SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut  = Generation().SignalPlain.TightCut
#tightCut.Decay = "[ Lambda_b0 ==>  ^(Lambda_c+ ==> ^p+ ^K- ^pi+)  ^mu- nu_mu~ ]CC"
# tightCut.Preambulo += [
# "from LoKiCore.functions import in_range"  ,
# "from GaudiKernel.SystemOfUnits import GeV, MeV",
# "pipiKP     = GCHILD(GP,1) + GCHILD(GP,2) + GCHILD(GP,3)" ,
# "pipiKPT     = GCHILD(GPT,1) + GCHILD(GPT,2) + GCHILD(GPT,3)" 
#  ]
# tightCut.Cuts      =    {
#'[p+]cc'   : " in_range( 0.010 , GTHETA , 0.400 )& ( GPT > 150 * MeV )" ,
#'[pi+]cc'   : " in_range( 0.010 , GTHETA , 0.400 )& ( GPT > 150 * MeV )" ,
#'[K+]cc'   : " in_range( 0.010 , GTHETA , 0.400 )& ( GPT > 150 * MeV )" ,
#'[mu-]cc'  : " in_range( 0.010 , GTHETA , 0.400 ) & (GP > 2500 * MeV) ",
#'[Lambda_c+]cc' : "(pipiKP > 15000 *MeV) & (pipiKPT > 2300 *MeV)"
#   }
# EndInsertPythonCode
#
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Patrick Owen
# Email: patrick.haworth.owen@cern.ch
# Date:   20170722
#
Alias MyLambda_c+       Lambda_c+
Alias Myanti-Lambda_c-  anti-Lambda_c-
ChargeConj MyLambda_c+  Myanti-Lambda_c-
#
Alias      MyK*0          K*0
Alias      Myanti-K*0     anti-K*0
ChargeConj MyK*0          Myanti-K*0
#
Alias      MyLambda(1520)0       Lambda(1520)0
Alias      Myanti-Lambda(1520)0  anti-Lambda(1520)0
ChargeConj MyLambda(1520)0       Myanti-Lambda(1520)0
#
Decay Lambda_b0sig
  1.0    MyLambda_c+        mu-  anti-nu_mu     PHOTOS   Lb2Baryonlnu  1 1 1 1;
Enddecay
CDecay anti-Lambda_b0sig
# BR = 1
Decay MyLambda_c+
# Lc->pKpi:
  0.02800         p+      K-      pi+          PHSP;
  0.016         p+      Myanti-K*0           PHSP;
  0.00860         Delta++ K-                   PHSP;
  0.00414         MyLambda(1520)0 pi+          PHSP;  
Enddecay
CDecay Myanti-Lambda_c-
# BR = 1
#
# 
#
Decay MyK*0
  1.0      K+  pi-                          VSS;
Enddecay
CDecay Myanti-K*0
#
#
Decay MyLambda(1520)0
  1.0   p+     K-                             PHSP;
Enddecay
CDecay Myanti-Lambda(1520)0
#
#
End
