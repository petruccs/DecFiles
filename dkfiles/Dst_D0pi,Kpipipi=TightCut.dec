# EventType: 27465006
#
# Descriptor: [D*(2010)+ -> (D0 -> K- pi- pi+ pi+) pi+]cc
#
# NickName: Dst_D0pi,Kpipipi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay = '^[D*(2010)+ => ^(D0 ==> ^K- pi- ^pi+ pi+) pi+]CC'
# tightCut.Cuts      =    {
#     '[pi+]cc'   : ' goodMuon  ' ,
#     '[K-]cc'    : ' goodKaon  ' ,
#     '[D0]cc'    : ' goodD     ' ,
#     '[D*(2010)+]cc'   : ' goodDst & goodSlowPion  ', }
# tightCut.Preambulo += [
#     'inAcc     = in_range ( 0.005 , GTHETA , 0.400 ) & in_range( 1.9 , GETA , 5.0 )' ,
#     'goodMuon  = ( GPT > 500  * MeV ) & ( GP > 6 * GeV )     & inAcc   ' ,
#     'goodKaon  = ( GPT > 300  * MeV ) & ( GP > 2 * GeV )     & inAcc   ' ,
#     'goodPion  = ( GPT > 150  * MeV )                        & inAcc   ' ,
#     'goodSlowPion  = GCHILDCUT (  ( GPT > 300  * MeV ) & ( GP > 1000  * MeV ) & inAcc , "Charm =>  Charm ^(pi+|pi-)"  )',
#     'goodD     = ( GPT > 250  * MeV ) & ( GP > 3 * GeV )', 
#     'goodDst   = ( GPT > 500  * MeV ) & ( GP > 1 * GeV )', ]
#
# EndInsertPythonCode
#
# Documentation: D*+ forced to decay to D0 pi+. Includes resonances in D0 decay. K*0 forced into K+ pi-. a1+ forced into rho pi+. omega forced into pi+ pi-. Tight cuts.
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Lorenzo Capriotti
# Email: lorenzo.capriotti@cern.ch
# Date: 20180926
# CPUTime: < 1 min
# 
#
Alias MyD0         D0
Alias Myanti-D0    anti-D0
ChargeConj MyD0    Myanti-D0
Alias Mya_1+       a_1+
Alias Mya_1-       a_1-
ChargeConj Mya_1+  Mya_1-
Alias MyK*0        K*0
Alias Myanti-K*0   anti-K*0
ChargeConj MyK*0   Myanti-K*0
Alias MyK_1-       K_1-
Alias MyK_1+       K_1+
ChargeConj MyK_1-  MyK_1+
Alias Myomega      omega
ChargeConj Myomega Myomega

Decay D*+sig
  1.000 MyD0 pi+  VSS;
Enddecay
CDecay D*-sig

# 
# Total D0 = 8.14%
#
# Sum of D0 subdecays = 8.37%
#

Decay MyD0
  0.0360      Mya_1+       K-            SVS;
  0.0106      Myanti-K*0   rho0          SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0001      Myanti-K*0   Myomega       SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0029      MyK_1-       pi+           SVS;
  0.0160      Myanti-K*0   pi+   pi-     PHSP;
  0.0051      K-  pi+   rho0             PHSP;
  0.0001      K-  pi+   Myomega          PHSP;
  0.0189      K-  pi+  pi+  pi-          PHSP;
Enddecay
CDecay Myanti-D0
#
# Total a_1+ = 0.492
#
Decay Mya_1+ 
  1.0000      rho0      pi+            VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay 
Decay Mya_1-
  1.0000      rho0      pi-            VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
#
#  Total K*0 = 0.6657
#
Decay MyK*0
  1.0000      K+        pi-            VSS;
Enddecay
CDecay Myanti-K*0
#
#  Total omega = 0.0221
#
Decay Myomega
  1.0000      pi+       pi-            VSS;
Enddecay
#
# Total K_1- = 0.4978
#
Decay MyK_1+
  0.2800      rho0      K+             VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.0710      MyK*0     pi+            VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.0024      Myomega   K+             VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.1444      K+    pi+    pi-         PHSP;
Enddecay
Decay MyK_1-
  0.2800      rho0      K-             VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.0710      Myanti-K*0  pi-          VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.0024      Myomega     K-           VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.1444      K-    pi-    pi+         PHSP;
Enddecay
#
End
#
