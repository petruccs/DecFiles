# EventType: 13574089
# 
# Descriptor: [B_s0 -> (D_s- -> (anti-K*0 -> K- pi+) mu- anti-nu_mu) e+ nu_e]cc
#
# NickName: Bs_Dsenu,Kstmunu=DecProdCut,HighVisMass,EvtGenDecayWithCut
# Cuts: DaughtersInLHCb
#
# InsertPythonCode:
# #
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
# #
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/HighVisMass"
# evtgendecay.addTool(LoKi__GenCutTool ,'HighVisMass')
# evtgendecay.HighVisMass.Decay   = '[^(B_s0 => ^(D_s- => ^(K*(892)~0 => ^K- ^pi+) ^mu- ^nu_mu~) ^e+ ^nu_e)]CC'
# evtgendecay.HighVisMass.Cuts    = { '[B_s0]cc' : "visMass" }
# evtgendecay.HighVisMass.Preambulo += [
#     "visMass = ( ( GMASS ( 'e-' == GABSID , 'mu-' == GABSID, 'K+' == GABSID, 'pi+' == GABSID ) ) > 4500 * MeV )",
# ]
# EndInsertPythonCode
#
# Documentation: background for B0 -> K* e mu LFV search
# selected to have a visible mass larger than 4.5 GeV using EvtGenDecayWithCutTool
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Andreas Guth
# Email: andreas.guth@cern.ch
# Date: 20180803
# CPUTime: < 1 min
#

Alias MyK*0   K*0 
Alias Myanti-K*0 anti-K*0 
ChargeConj MyK*0  Myanti-K*0 

Alias MyD_s+ D_s+
Alias MyD_s- D_s-
ChargeConj MyD_s+ MyD_s-

Decay B_s0sig
  1.000        MyD_s- e+ nu_e  PHOTOS HQET2 1.185 1.081;
Enddecay
CDecay anti-B_s0sig
#
Decay MyD_s-
1.000         Myanti-K*0 mu- anti-nu_mu PHOTOS ISGW2;
Enddecay
CDecay MyD_s+
#
Decay Myanti-K*0  
1.000          K- pi+         VSS;
Enddecay           
CDecay MyK*0
#
End
#
