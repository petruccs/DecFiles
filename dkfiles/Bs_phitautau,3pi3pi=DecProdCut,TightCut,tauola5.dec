# EventType: 13102040
#
# Descriptor: {[[B_s0]nos -> (tau+ -> pi+ pi- pi+ anti-nu_tau) (tau- -> pi+ pi- pi- nu_tau) (phi(1020) -> K+ K-)]cc, [[B_s0]os -> (tau- -> pi+ pi- pi- nu_tau)(tau+ -> pi+ pi- pi+ anti-nu_tau) (phi(1020) -> K+ K-)]cc}
#
# NickName: Bs_phitautau,3pi3pi=DecProdCut,TightCut,tauola5
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#gen = Generation()
#gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
#tightCut = gen.SignalRepeatedHadronization.TightCut
#tightCut.Decay = "[ (Beauty) ==>  ^(phi(1020) -> ^K+ ^K-)   ^(tau+ ==> ^pi+ ^pi- ^pi+ nu_tau~) ^(tau- ==> ^pi- ^pi+ ^pi- nu_tau) ]CC"
#tightCut.Preambulo += [
#  "from LoKiCore.functions import in_range"  ,
#  "from GaudiKernel.SystemOfUnits import GeV, MeV"  
#]
#tightCut.Cuts      =    {
# '[pi+]cc'   : " in_range( 0.010 , GTHETA , 0.400 ) & ( GPT > 250 * MeV )  " ,
# '[K-]cc'   : "  in_range( 0.010 , GTHETA , 0.400 )  & ( GPT > 250 * MeV )" 
#    }
# EndInsertPythonCode

# Documentation: Bd decay to phi tau tau.
# Both tau leptons decay in the 3-prong charged pion mode using latest Tauola BaBar model.
# All final-state products in the acceptance.
# EndDocumentation
#
# PhysicsWG: RD
#
# CPUTime: < 1 min
# Tested: Yes
# Responsible: Luca Pescatore
# Email: luca.pescatore@cern.ch
# Date: 20181001
#

# Tauola steering options
Define TauolaCurrentOption 0
Define TauolaBR1 1.0
#
Alias         Mytau+     tau+
Alias         Mytau-     tau-
ChargeConj    Mytau+     Mytau-
Alias         MyPhi      phi
ChargeConj    MyPhi      MyPhi
#
Decay B_s0sig
  1.000       MyPhi      Mytau+    Mytau-       BTOSLLBALL;
Enddecay
CDecay anti-B_s0sig
#
Decay MyPhi
  1.000       K+         K-       VSS;
Enddecay
#
Decay Mytau-
  1.00        TAUOLA 5;
Enddecay
CDecay Mytau+
#
#
End
