# Deadline for next release

The release plan with deadline for DecFiles over coming months is:

  1. DecFiles v30r28 with deadline on Friday 11th January 2019 at 14:00.
  1. DecFiles v30r29 with deadline on Friday 25th Janary 2019 at 14:00.

Usually plan is to release on Monday or Tuesday after deadline. Merge requests created after deadline are not guaranteed to be accepted for this release.

# Guide for submitting decay file
Please follow [Contribution guide](https://gitlab.cern.ch/lhcb-datapkg/Gen/DecFiles/blob/master/CONTRIBUTING.md) on top of the page for instructions on how to prepare, test and commit decay file.

