BEGIN{
  cnt = 0
}

/FAILED/{
  sub("\\.FAILED", "", $1)
  failures[cnt] = $1
  cnt += 1
}

END{
  if(cnt){
    print "Failures:"
    for(i in failures) {
      print "-", failures[i]
    }
# For now ignore any failures on exit, just list failed files.
    exit(0)
  } else {
    print "everything OK"
  }
}
