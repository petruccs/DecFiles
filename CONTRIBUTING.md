# Steps to add decay file
1. Find if a DecFile you want (or similar enough so you can use it) already exists
1. Read the [event type numbering convention](https://cds.cern.ch/record/855452?ln=en) or check out [the (unofficial) event type finder](http://eventtype.boosted.science/).
1. If you are using an old DecFile as a template, note that it might not follow the established conventions.
1. Test your DecFile (both that it runs and that it produces output you want)
1. After you've modified the release notes in the doc folder of the package commit to branch and create merge request against master.
1. Open your merge request in web browser and check that all tests are successful. If anything fails, please correct and recheck again day after commit. If you do not understand failure, get in touch with [lhcb-gauss-manager@cernNOSPAMPLEASE.ch](mailto://lhcb-gauss-manager@cernNOSPAMPLEASE.ch)
1. Watch discussion in merge request for any comments we might have.


# Testing a decay file
 This is done in three steps:
## Write the decay file and create the options
If you do not already have the environment set up, prepare it to run one of the latest Gauss version (v49rX for Sim09 productions, or v50rX for upgrade productions). In the examples below, v49r9 is used.
```shell
LbLogin -c x86_64-slc6-gcc49-opt 
lb-dev Gauss/v49r11
cd ./GaussDev_v49r11
```
Then get the Gen/DecFiles package from gitlab.
```shell
git lb-clone-pkg  Gen/DecFiles 
```
Add the new decay file in the dkfiles subdirectory and create the options to be used by Gauss.
```shell
make install
```
If there is no mistake in the decay file, an option file called NNNNNNNN.py is present in the options subdirectory, where NNNNNNNN is the event type number of the decay file.  It is also recommended to run the decay file convention parser
```shell
cd Gen/DecFiles/cmt
./decparser.sh ../dkfiles/DecFile.dec
```
This should check for convention correctness for the event type and some basic mistakes, but it also has some limitations. This is our tool to help in checking, but at the end all failures are individually considered. Feel free to email any comments to lhcb-gauss-manager.  
## Run Gauss to create a .xgen file 
The .xgen file contains the generator level information (both in HepMC and MCParticles format) when running Gauss with only the generator part, without the simulation and Geant4 being activated.
```shell
LbLogin -c x86_64-slc6-gcc49-opt 
./run bash
gaudirun.py $GAUSSOPTS/Gauss-Job.py $GAUSSOPTS/Gauss-2016.py $GAUSSOPTS/GenStandAlone.py \
            $DECFILESROOT/options/NNNNNNNN.py $LBPYTHIA8ROOT/options/Pythia8.py
```

The number of events to generate (5 by default) is set in the Gauss-Job.py file.  Gauss will then produce a file called by default Gauss-NNNNNNNN-5ev-YYYYMMDD.xgen, with a name formed from the event type (NNNNNNNN), the number of events (5ev by default) and the day (in format YYYYMMDD).  
The above command includes spillover, which you should use for timing of your event type. If for development reasons you want to remove spillover, you can use $GAUSSOPTS/Gauss-Dev.py instead of $GAUSSOPTS/Gauss-2016.py.
## Create a MCDecayTreeTuple from the .xgen file
Use DaVinci to read the file produced by Gauss and to create a ROOT file with the information from the generator level history.
```shell
lb-run DaVinci gaudirun.py tupleResult.py
```
where tupleResult.py contains
```python
from Configurables import (
    DaVinci,
    EventSelector,
    PrintMCTree,
    MCDecayTreeTuple
)
from DecayTreeTuple.Configuration import *

"""Configure the variables below with:
decay: Decay you want to inspect, using 'newer' LoKi decay descriptor syntax,
decay_heads: Particles you'd like to see the decay tree of,
datafile: Where the file created by the Gauss generation phase is, and
year: What year the MC is simulating.
"""

# https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders
decay = "[B0 => ^(Lambda_c~- ==> ^p~- ^K+ ^pi-) ^p+ ^pi- ^pi+]CC"
decay_heads = ["B0", "B~0"]
datafile = "Gauss-11166070-5ev-20140429.xgen"
year = 2012

# For a quick and dirty check, you don't need to edit anything below here.
##########################################################################

# Create an MC DTT containing any candidates matching the decay descriptor
mctuple = MCDecayTreeTuple("MCDecayTreeTuple")
mctuple.Decay = decay
mctuple.ToolList = [
    "MCTupleToolHierarchy",
    "LoKi::Hybrid::MCTupleTool/LoKi_Photos"
]
# Add a 'number of photons' branch
mctuple.addTupleTool("MCTupleToolKinematic").Verbose = True
mctuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Photos").Variables = {
    "nPhotos": "MCNINTREE(('gamma' == MCABSID))"
}

# Print the decay tree for any particle in decay_heads
printMC = PrintMCTree()
printMC.ParticleNames = decay_heads

# Name of the .xgen file produced by Gauss
EventSelector().Input = ["DATAFILE='{0}' TYP='POOL_ROOTTREE'
Opt='READ'".format(datafile)]

# Configure DaVinci
DaVinci().TupleFile = "DVntuple.root"
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().DataType = str(year)
DaVinci().UserAlgorithms = [printMC, mctuple]
```
If you get the error
```
ToolSvc.OdinTim...  FATAL ODINDecodeTool:: Exception throw: Cannot find RawEvent in [Trigger/RawEvent, DAQ/RawEvent] StatusCode=FAILURE
```
try adding this to your file (copied from https://gitlab.cern.ch/lhcb/Bender/blob/master/Phys/BenderTools/python/BenderTools/GenFiles.py):
```python
def doIt():
    """
    specific post-config action for (x)GEN-files 
    """
    extension = "xgen"
    ext = extension.upper()

    from Configurables import DataOnDemandSvc
    dod  = DataOnDemandSvc ()
    from copy import deepcopy 
    algs = deepcopy ( dod.AlgMap ) 
    bad  = set() 
    for key in algs :
        if     0 <= key.find ( 'Rec'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Raw'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'DAQ'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Trigger' )                  : bad.add ( key )
        elif   0 <= key.find ( 'Phys'    )                  : bad.add ( key )
        elif   0 <= key.find ( 'Prev/'   )                  : bad.add ( key )
        elif   0 <= key.find ( 'Next/'   )                  : bad.add ( key )
        elif   0 <= key.find ( '/MC/'    ) and 'GEN' == ext : bad.add ( key )
        
    for b in bad :
        del algs[b]
            
    dod.AlgMap = algs
    
    from Configurables import EventClockSvc, CondDB 
    EventClockSvc ( EventTimeDecoder = "FakeEventTime" )
    CondDB  ( IgnoreHeartBeat = True )
    
appendPostConfigAction( doIt )
```

# Efficiency of generator level cuts
This number can be extracted from the GeneratorLog.xml file (produced by your gauss job), where the printout is of the form
```xml
<efficiency name = "generator level cut">
    <after> 5 </after>
    <before> 27 </before>
    <value> 0.18519 </value>
    <error> 0.074757 </error>
  </efficiency>
```
the efficiency is therefore 5/27 in this example.


# Commiting a decay file
Pre-requisite: have account on gitlab.cern.ch 
You must commit your decay file to separate branch and create merge request against master branch. It is recommended that you check out a clean version
```shell
git lb-clone-pkg  Gen/DecFiles
cd Gen/DecFiles
git checkout -b ${USER}/my-changes
```
Copy your decfile(s) into `dkfiles/` and add them to commit
```shell
git add dkfiles/[your decfile]
```
Commit everything, push to the gitlab server
```shell
git status #(check that release.notes is updated and your decfile is added)
git commit #(with a commit message of what you updated the release notes with)
git push -u origin ${USER}/my-changes
```
Go to [gitlab.cern.ch](https://gitlab.cern.ch/lhcb-datapkg/Gen/DecFiles) and create merge request. In description of merge request input text suitable for release notes. 
Format of it can be seen in existing doc/release.notes.

# Automatic testing of a decay file
When you create merge request or update with new commit, automatic test will be performed. You can find results of the test directly on gitlab page of your merge request under text like "Pipeline #nnnnnnn passed/failed ..." You can click on it and see details of all tests. Three things are done in this test, generating option files, running decparser and running Gauss itself for few events. If you see a failure, please fix issue. If you do not understand failure, get in touch with [lhcb-gauss-manager@cernNOSPAMPLEASE.ch](mailto://lhcb-gauss-manager@cernNOSPAMPLEASE.ch). The merge requests cannot be merged until it passes tests and do not assume that somebody will remind that it needs to be done (or fix it behind the scenes).

